//
//  HomeNavigator.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation

final class HomeNavigator: Navigator {
  func setup() -> HomeViewController {
    let homeVC = HomeViewController.initFromNib()
    homeVC.viewModel = HomeViewModel(navigator: self, useCases: servicePackage.dataAccess.getHomeUseCases())
    homeVC.viewModel?.loadData()
    return homeVC
  }
  
  func openDetails(vm: BookCellViewModel) {
    let navigator = BookDetailsNavigator(navigationController: navigationController, servicePackage: servicePackage)
    navigationController.pushViewController(navigator.setup(symbol: vm.symbol), animated: true)
  }
}
