//
//  HomeViewController.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/16/22.
//

import UIKit

class HomeViewController: ViewController {
  
  @IBOutlet weak var booksTableView: UITableView!
  let refreshControl = UIRefreshControl()
  var viewModel: HomeViewModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    loadData()
  }

  func setupUI(){
    refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
    refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
    booksTableView.addSubview(refreshControl)
    booksTableView.register(UINib(nibName: "BookTableViewCell", bundle: nil), forCellReuseIdentifier: "bookCellId")
    booksTableView.rowHeight = 64
    booksTableView.dataSource = self
    booksTableView.delegate = self
  }
  
  @objc func refresh(_ sender: AnyObject) {
    loadData()
  }
  
  func loadData() {
    viewModel?.loadData(completion: { [unowned self] (list, error) in
      DispatchQueue.main.async {
        self.refreshControl.endRefreshing()
        self.booksTableView.reloadData()
      }
    })
  }
}

extension HomeViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel?.bookList.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let vm = viewModel?.bookList[indexPath.row], let cell = tableView.dequeueReusableCell(withIdentifier: "bookCellId") as? BookTableViewCell {
      cell.setViewModel(vm: vm)
      return cell
    }
    return UITableViewCell()
  }
}

extension HomeViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let vm = viewModel?.bookList[indexPath.row]{
      viewModel?.navigator.openDetails(vm: vm)
    }
  }
}
