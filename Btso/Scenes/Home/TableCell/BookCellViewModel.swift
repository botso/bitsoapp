//
//  BookCellViewModel.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/16/22.
//

import Foundation
import Domain
class BookCellViewModel {
  
  var priceDelegate: PriceUpdateProtocol?
  let symbol: String
  let useCase: BookDetailsUseCases
  let model: Book
  var lastPrice: String?
  init(model: Book, useCase: BookDetailsUseCases) {
    self.symbol = model.book
    self.model = model
    self.useCase = useCase
  }
  
  func updatePrice() {
    useCase.getDetails(request: BookDetailIO.Request(bookSymbol: model.book)) { [unowned self] res in
      print(res)
      switch res {
      case .success(let response):
        self.lastPrice = response.last
        self.priceDelegate?.updatePrice(price: response.last)
      case .failure(let error):
        print("unhandled error", error)
      }
    }.resume()
  }
}
