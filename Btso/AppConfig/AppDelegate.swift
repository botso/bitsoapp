//
//  AppDelegate.swift
//  Btso
//
//  Created by Behrad Kazemi on 6/16/22.
//

import UIKit
import Platforms

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    Application.shared.configureMainInterface(in: window!)
    return true
  }
}

