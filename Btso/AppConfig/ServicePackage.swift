//
//  ServicePackage.swift
//  ClientApp
//
//  Created by Behrad Kazemi on 6/10/22.
//

import Foundation
import Domain

final public class ServicePackage {
  public let dataAccess: UseCaseProvider
  
  public init(dataAccess: UseCaseProvider) {
    self.dataAccess = dataAccess
  }
}
